minetest.register_craft({
  type = 'shapeless',
  recipe =  {'default:dirt'},
  output = 'natural_soil:natural_soil',
})

minetest.register_craft({
  type = 'shapeless',
  recipe =  {'group:leaves'},
  output = 'default:stick 1',
})

minetest.register_craft({
  type = 'shapeless',
  recipe = {'natural_soil:natural_soil'},
  output = 'natural_soil:rock 1',
	replacements = {{'natural_soil:natural_soil', 'natural_soil:soil_pile'}},
})
minetest.register_craft({
  type = 'shapeless',
  recipe = {'natural_soil:soil_pile'},
  output = 'default:stick 1',
	replacements = {{'natural_soil:soil_pile', 'natural_soil:soil'}},
})
minetest.register_craft({
  type = 'shapeless',
  recipe = {'natural_soil:soil'},
  output = 'natural_soil:organic_material 1',
	replacements = {{'natural_soil:soil', 'natural_soil:grit'}},
})

minetest.register_craft({
  type = 'shapeless',
  recipe = {'natural_soil:organic_material', 'more_realistic_tools:grind_stone'},
  output = 'default:seeds 1',
	replacements = {
    {'natural_soil:organic_material', 'natural_soil:organic_powder'},
    {'more_realistic_tools:grind_stone', 'more_realistic_tools:grind_stone'}
  },
})
