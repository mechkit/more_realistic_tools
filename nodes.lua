minetest.register_node("natural_soil:natural_soil", {
	description = "Natural Soil",
	tiles = {"natural_soil_natural_soil.png"},
	groups = {crumbly = 3, soil = 1},
	sounds = default.node_sound_dirt_defaults(),
})
