local modpath = minetest.get_modpath("natural_soil")

-- Load files
dofile(modpath.."/nodes.lua")
dofile(modpath.."/tools.lua")
dofile(modpath.."/craftitems.lua")
dofile(modpath.."/crafting.lua")
dofile(modpath.."/fuel.lua")
--dofile(modpath.."/mapgen.lua")
--dofile(modpath.."/aliases.lua")
