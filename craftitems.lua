

minetest.register_craftitem('natural_soil:soil_pile', {
  description = 'Soil Pile',
	-- groups = {stone = 1},
  inventory_image = 'natural_soil_soil_pile.png',
})
minetest.register_craftitem('natural_soil:soil', {
  description = 'Soil',
	-- groups = {stone = 1},
  inventory_image = 'natural_soil_soil.png',
})
minetest.register_craftitem('natural_soil:organic_material', {
  description = 'Organic Material',
  -- groups = {stone = 1},
  inventory_image = 'natural_soil_organic_material.png',
})
minetest.register_craftitem('natural_soil:organic_powder', {
  description = 'Organic Powder',
	-- groups = {stone = 1},
  inventory_image = 'natural_soil_organic_powder.png',
})
minetest.register_craftitem('natural_soil:grit', {
  description = 'Grit',
	-- groups = {stone = 1},
  inventory_image = 'natural_soil_grit.png',
})

minetest.register_craftitem('natural_soil:rock', {
  description = 'Stone',
	-- groups = {stone = 1},
  inventory_image = 'natural_soil_stone.png',
})
